<?php

namespace App\Http\Controllers;
use App\Vueaxios;
use App\Http\Resources\AxiosResource;

use Illuminate\Http\Request;

class AxiosController extends Controller
{
    public function index(){
        $vueaxios = Vueaxios::all();

        return AxiosResource::collection($vueaxios);
    }

    public function store(Request $request){
        $vue = Vueaxios::create([
            'name' => $request->name,
        ]);
        return $vue;
    }   

    public function update(Request $request ,$id){
        $vues = Vueaxios::find($id);
        $vues->name = $request['name'];
        dd($request['name']);
        // $vues->save();
        // $vues = Vueaxios::where('id', $id)->update(['name' => $request->name]);
       
        // return $vues; 

    }

    public function delete($id){
        Vueaxios::destroy($id);
        return 'Delete Sukses';
    }



}
