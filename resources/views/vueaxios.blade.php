<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vue JS | Latiham</title>
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>



</head>

<body>
    <div id="app">
        <form>
            <input type="text" v-model="form.name"></input>
            <button @click.prevent="addUser" v-show="!updateSubmit"> Add </button>
            <button @click.prevent="updateUser" v-show="updateSubmit"> Update </button>
        </form>
        <ul v-for="(user, index) in users">
            <li>
                @{{ user.name }}
                <button @click="editUser(user , index )"> Edit </button> ||
                <button @click="removeUser(index , user)">
                    Delete
                </button>

            </li>
        </ul>
    </div>

    {{-- <!-- <template id="user-template">
        <div class="">
            <div>{{ user.name }} {{ user.index }}
                <button @click="editUser(user , index)"> Edit </button> ||
                <button @click="removeUser(index)">
                    Delete
                </button>
            </div>
            <br>

        </div>

    </template> --> --}}

    <script>
        Vue.component('users', {
            template: '#user-template',
            props: ['user']
        })


      new Vue({
          
            el: '#app',
            data() {
                return {
                    users: [],
                    updateSubmit: false,
                    form: {'name': ''},
                    selectedIdUser: null
                }

            },
            methods: {
                addUser() {
                    let textInput = this.form; 
                    this.$http.post('/api/vueaxios/create', textInput).then(response => {
                        this.users.push(
                        textInput
                    )
                });
                   
                    this.form = {}
                },
                editUser(user, index) {
                        this.selectedIdUser = index
                        this.updateSubmit = true,
                        this.form.name = user.name
                },
                updateUser() {
                        let textInput = this.form; 
                        // console.log(textInput)

                        this.$http.put('/api/update/', textInput).then(response => {
                        this.users[this.selectedIdUser].name = this.form.name
                        });                
                        this.form = {}
                        this.updateSubmit = false,
                        this.selectedIdUser = null
                },
                removeUser(index , user) {
                    var r = confirm('Anda Yakin??')
                    if (r) 
                    {
                        this.$http.delete('/api/delete/' + user.id).then(response => 
                        {
                        this.users.splice(index, 1)
                        });
                    }
                }
            },
            mounted(){
             axios.get('/api/vueaxios').then(response => {
                    this.users = response.data;
                    console.log(this.users);
                });  


                // this.$http.get('/api/vueaxios').then(response => {
                //     let result = response.body.data;
                //     this.users = result;
                // });
        }
    });
    </script>
</body>

</html>